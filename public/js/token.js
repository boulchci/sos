document.body.onload = function () {
    let form_token = document.getElementById('token');
    fetch('/token').then(function(response) {
        response.json().then(function(token) {
            //console.log(token);
            form_token.value = JSON.stringify(token);
        });
    });
};