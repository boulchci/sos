const API_KEY_OPENCAGEDATA = "333c2d590cb74f02a3599d337bafdf20";
const POS_ENSSAT = [48.7297929, -3.4625479];
const KM_ENSSAT_AUTO = 5.5 * 1000;
let map;
let marker;

(function (window, document) {

    /* Gestion du menu */ 
    var layout   = document.getElementById('layout'),
        menu     = document.getElementById('menu'),
        menuLink = document.getElementById('menuLink'),
        content  = document.getElementById('main');

    let toggleClass = function(element, className) {
        var classes = element.className.split(/\s+/),
            length = classes.length,
            i = 0;

        for(; i < length; i++) {
          if (classes[i] === className) {
            classes.splice(i, 1);
            break;
          }
        }
        // The className is not found
        if (length === classes.length) {
            classes.push(className);
        }

        element.className = classes.join(' ');
    }

    let toggleAll = function(e) {
        var active = 'active';

        e.preventDefault();
        toggleClass(layout, active);
        toggleClass(menu, active);
        toggleClass(menuLink, active);
    }

    menuLink.onclick = function (e) {
        toggleAll(e);
    };

    content.onclick = function(e) {
        if (menu.className.indexOf('active') !== -1) {
            toggleAll(e);
        }
    };

    /* Gestion de la carte */
    /* Ecoute de la bar recherche d'adresse */
    if(document.getElementById("map") != null && document.getElementById("adresse") != null){
        map = initMap();
        document.getElementById("adresse").onkeyup = (ev => ecouteAdresseInput(ev));
    }


    /* Gestion du formulaire */
    if(document.getElementById("formulaire") != null && document.getElementById("nature_commande") != null && document.getElementById("command_type") != null){

        document.getElementById("formulaire").onsubmit = gestionFormulaire;
        
        // Ecoute des radios
        for(let radiobt of document.getElementsByName("nature_commande")){
            radiobt.onchange = gestionRadioBt;

            if(radiobt.checked)
                remplirSelect(radiobt.value);
        }

        
    }

}(this, this.document));

function initMap(){

    map = L.map('map').setView(POS_ENSSAT, 13);

    var circle = L.circle(POS_ENSSAT, KM_ENSSAT_AUTO, {
        color: 'red',
        fillColor: '#f03',
        fillOpacity: 0.05
    }).addTo(map);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    return map;
}

let timeout_ecouteAdresseInput; // Var globale
function ecouteAdresseInput(ev){

    if(ev.target.value.length > 7){ // Plus de 10, sinon inutile

        clearTimeout(timeout_ecouteAdresseInput);

        timeout_ecouteAdresseInput = setTimeout(function () {       

            let adresseURL = encodeURI(ev.target.value);

            fetch(`https://api.opencagedata.com/geocode/v1/json?key=333c2d590cb74f02a3599d337bafdf20&q=${adresseURL}`)
            .then(reponse => reponse.json())
            .then(reponse => {

                if(reponse.results.length > 0){

                    if(typeof marker !== 'undefined')
                        map.removeLayer(marker);

                    marker = L.marker([reponse.results[0].geometry.lat, reponse.results[0].geometry.lng]).addTo(map);

                    map.flyTo([reponse.results[0].geometry.lat, reponse.results[0].geometry.lng], 18);  
                }
            });

        }, 500);

    }
}

function gestionFormulaire(ev){

    if(typeof marker !== 'undefined'){

        let marker_input = document.getElementById('marker');
        marker_input.value = JSON.stringify(marker);

        // On cherche le radio coché
        for (let radio of document.getElementsByName("nature_commande")) {
            if(radio.checked){
                ev.target.action = radio.value;
                break;
            }
        }

        // Verif du nombre de kilometre
        let distance = get_distance_m(marker.getLatLng().lat, marker.getLatLng().lng, POS_ENSSAT[0], POS_ENSSAT[1]);
        if(distance > KM_ENSSAT_AUTO){

            alert("Ton domicile est trop loin pour nous ("+distance/1000+" km)! Fais attention à ce que l'adresse ne dépasse par 5,5 km depuis l'ENSSAT.");
            ev.preventDefault();
            return;
        }

        // Création de champs latlng pour POST
        let laglngInput = document.createElement("input");
        laglngInput.name = "latlng";
        laglngInput.value = marker.getLatLng().lat+","+marker.getLatLng().lng;
        laglngInput.style.display = "hide";

        ev.target.submit();
        return;
    }

    ev.preventDefault();
}

function gestionRadioBt(ev){

    if(ev.target.value != "livraison"){
        for(let element of document.getElementsByClassName("dominos_div")){
            element.style.display = "none";
            document.getElementById("command_name").required = false;
        }
    }else{

        for(let element of document.getElementsByClassName("dominos_div")){
            element.style.display = "block";
            if(element.tagName == "input"){
                document.getElementById("command_name").required = true;
            }
        }        
    }


    remplirSelect(ev.target.value);
}

function remplirSelect(nature){

    let select = document.getElementById("command_type");

    if(nature == "service"){

        fetch("/services").then(rep => rep.json()).then(rep => {

            removeOptions(select);

            for (element of rep) {

                addOptionSelect(select, element.id, element.name);
            }
        });

    }else if(nature == "livraison"){

        fetch("/livraisons").then(rep => rep.json()).then(rep => {

            removeOptions(select);

            for (element of rep) {

                addOptionSelect(select, element.id, element.name);
            }
        });
    }

}

function addOptionSelect(select, value, name){

    let option = document.createElement("option");
    option.text = name;
    option.value = value;

    select.add(option);
}

function removeOptions(selectbox){
    var i;
    for(i = selectbox.options.length - 1 ; i >= 0 ; i--)
    {
        selectbox.remove(i);
    }
}

function deg2rad(x){
    return Math.PI*x/180; 
}
 
function get_distance_m($lat1, $lng1, $lat2, $lng2) {
    $earth_radius = 6378137;   // Terre = sphère de 6378km de rayon
    $rlo1 = deg2rad($lng1);    // CONVERSION
    $rla1 = deg2rad($lat1);
    $rlo2 = deg2rad($lng2);
    $rla2 = deg2rad($lat2);
    $dlo = ($rlo2 - $rlo1) / 2;
    $dla = ($rla2 - $rla1) / 2;
    $a = (Math.sin($dla) * Math.sin($dla)) + Math.cos($rla1) * Math.cos($rla2) * (Math.sin($dlo) * Math.sin($dlo));
    $d = 2 * Math.atan2(Math.sqrt($a), Math.sqrt(1 - $a));
    return ($earth_radius * $d);
}