const Discord = require('discord.js');
const bot = new Discord.Client();

bot.on('ready', function () {
    console.log('discord connected');
});

bot.login('NjcxNzY3MjEwNzI0ODE4OTQ3.XjBxKQ.bHqWbZp2upH0lm-kcrohLaC8D6c');


var discord = {
    send_message_dominos: function (id, real_name, command_name, address, phone, commentary) {
        bot.channels.get('672905458213847040').send(
            'Commande Dominos n°'+id+'\n'+
            real_name + ' a commandé au nom de ' + command_name + '\n' +
            'Il faut livrer la pizza ici : ' + address + '\n' +
            'Infos supplémentaires : '+commentary+'\n'+
            'tel : ' + phone+'\n'+
            '_________________________________');
    },
    send_message_kebab: function (id, real_name, command_name, address, phone, commentary) {
        bot.channels.get('672905458213847040').send(
            'Commande Kebab n°'+id+'\n'+
            real_name + ' a commandé au nom de ' + command_name + '\n' +
            'Il faut livrer le kebab ici : ' + address + '\n' +
            'Infos supplémentaires : '+commentary+'\n'+
            'tel : ' + phone+'\n'+
            '_________________________________');
    },
    send_message_vaisselle: function (id, real_name, address, phone, commentary) {
        bot.channels.get('672905458213847040').send(
            'Commande Vaisselle n°'+id+'\n'+
            real_name + ' veut qu\'on fasse sa vaisselle ' + '\n' +
            'Il faut aller ici : ' + address + '\n' +
            'Infos supplémentaires : '+commentary+'\n'+
            'tel : ' + phone+'\n'+
            '_________________________________');
    },
    send_message_gare: function (id, real_name, address, phone, commentary) {
        bot.channels.get('672905458213847040').send(
            'Commande Gare n°'+id+'\n'+
            real_name + ' veut qu\'on le/la dépose à la gare ' + '\n' +
            'Il faut le/la récupérer ici : ' + address + '\n' +
            'Infos supplémentaires : '+commentary+'\n'+
            'tel : ' + phone+'\n'+
            '_________________________________');
    },
    send_message_menage: function (id, real_name, address, phone, commentary) {
        bot.channels.get('672905458213847040').send(
            'Commande Ménage n°'+id+'\n'+
            real_name + ' veut qu\'on fasse son ménage ' + '\n' +
            'Il faut aller ici : ' + address + '\n' +
            'Infos supplémentaires : '+commentary+'\n'+
            'tel : ' + phone+'\n'+
            '_________________________________');
    },
    send_message_mystere: function (id, real_name, address, phone, commentary) {
        bot.channels.get('672905458213847040').send(
            'Commande Mystère n°'+id+'\n'+
            real_name + ' veut un SOS mystère' + '\n' +
            'Il faut aller ici : ' + address + '\n' +
            'Infos supplémentaires : '+commentary+'\n'+
            'tel : ' + phone+'\n'+
            '_________________________________');
    },
    send_message_apero: function (id, real_name, address, phone, commentary) {
        bot.channels.get('672905458213847040').send(
            'Commande Apéro n°'+id+'\n'+
            real_name + ' veut un apéro '+'\n' +
            'Il faut aller ici : ' + address + '\n' +
            'Infos supplémentaires : '+commentary+'\n'+
            'tel : ' + phone+'\n'+
            '_________________________________');
    },
    send_message_courses: function (id, real_name, address, phone, commentary) {
        bot.channels.get('672905458213847040').send(
            'Commande Courses n°'+id+'\n'+
            real_name + ' veut qu\'on lui livre ses courses '+'\n' +
            'Il faut aller ici : ' + address + '\n' +
            'Infos supplémentaires : '+commentary+'\n'+
            'tel : ' + phone+'\n'+
            '_________________________________');
    }
};


module.exports = discord;