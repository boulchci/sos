const { Pool } = require('pg');
const pool = new Pool({
    user: 'boulchci',
    host: '83.197.144.181',
    database: 'enssat',
    password: 'enssat22300',
    port: 5432,
});


var sql = {
    insert_dominos: function (livraison_id, real_name, command_name, address, phone, commentary) {
        return get_command_id(pool)
            .then(function (res) {
                id = res.rows[0].count;
                insert_commande(pool, id)
                    .then(function (success) {
                        insert_livraison(pool, id, livraison_id,real_name, command_name, address, phone, commentary);
                    });
                return id;
            })
    },
    get_livraisons: function(){
        return get_livraisons_enum(pool)
            .then(function (result) {
                return result.rows;
            });
    },
    get_services: function(){
      return get_services_enum(pool)
          .then(function (result) {
                return result.rows;
          });
    },
    insert_service: function (service_id,real_name, address, phone, commentary) {
        return get_command_id(pool)
            .then(function (res) {
                id = res.rows[0].count;
                insert_commande(pool, id)
                    .then(function (success) {
                        insert_service(pool, id, service_id, real_name, address, phone, commentary)
                    });
                return id;
            });
    },
    create_token: function (token_value) {
        return get_token_id(pool)
            .then(function (res) {
                id = res.rows[0].count;
                insert_token(pool, id, token_value);
                return id;
            });
    },
    validate_token: function (token_value, token_id) {
        return is_token_valid(pool, token_value, token_id)
            .then(function (res) {
                let is_valid = false;
                if(res.rows[0].count == 1){
                    is_valid = true;
                    set_token_validated(pool, token_id);
                }
                return is_valid;
            })
    },
    clear_tokens: function (delai) {
        console.log('clear');
        setInterval(function () {
            console.log('intercal');
            delete_tokens(pool);
        }, delai);
    }
};

function get_command_id(pool) {
    return pool.query('SELECT COUNT(*) FROM sos.commande');
}

function insert_commande(pool, id) {
    return pool.query('INSERT INTO sos.commande VALUES ($1,0,false)', [id]);
}

function insert_livraison(pool, id, livraison_id, real_name, command_name, address, phone, commentary) {
    pool.query('INSERT INTO sos.livraison VALUES ($1,$2,$3,$4,$5,$6,$7)', [id, livraison_id,real_name, command_name, address, phone, commentary], (err, result) => {
        if (err) {
            return console.error('error insert dominos', err.stack)
        }
    });
}

function insert_service(pool, id, service_id,real_name, address, phone, commentary){
    console.log(id, service_id,real_name, address, phone, commentary);
    return pool.query('INSERT INTO sos.service VALUES ($1,$2,$3,$4,$5,$6)', [id, service_id, real_name, address, phone, commentary]);
}

function get_token_id(pool) {
    return pool.query('SELECT COUNT(*) FROM sos.token');
}

function insert_token(pool, id, token_value){
    pool.query('INSERT INTO sos.token values ($1, $2, false)',[id, token_value], (err, res)=>{
        if(err){
            return console.log('error insert token', err.stack);
        }
    });
}

function is_token_valid(pool, token_value, token_id){
    return pool.query('' +
        'SELECT COUNT(*) ' +
        'FROM sos.token ' +
        'WHERE id = $1' +
        ' AND value = $2' +
        ' AND validated = false',[token_id, token_value]);
}

function set_token_validated(pool, token_id){
    pool.query('UPDATE sos.token SET validated = true WHERE id=$1',[token_id],(err, res)=>{
        if(err){
            return console.log('error set_token', err.stack);
        }
    })
}

function delete_tokens(pool){
    pool.query('TRUNCATE TABLE sos.token')
}

function get_services_enum(pool){
    return pool.query('SELECT * FROM sos.service_enum');
}

function get_livraisons_enum(pool){
    return pool.query('SELECT * FROM sos.livraison_enum');
}

module.exports = sql;