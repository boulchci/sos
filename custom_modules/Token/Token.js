var sql = require('../Sql/Sql');

var token = {
    create : function () {
        let token_value = Math.round(Math.random() * 100000000000000);
        return sql.create_token(token_value)
            .then(function (token_id) {
                return {token_id : token_id, token_value : token_value};
            })
    },
    validate : function (token) {
        return sql.validate_token(token.token_value, token.token_id);
    }
};

module.exports = token;