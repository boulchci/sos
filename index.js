const DELAI = 900000;
var express = require('express');
var bodyParser = require("body-parser");
var session = require('express-session');
var app = express();
//var io = require('socket.io').listen(http);
var sql = require('./custom_modules/Sql/Sql');
var discord = require('./custom_modules/Discord/Discord');
var token = require('./custom_modules/Token/Token');
const fs = require('fs');

// Certificate
const privateKey = fs.readFileSync('/etc/letsencrypt/live/sos.athenssat.fr/privkey.pem', 'utf8');
const certificate = fs.readFileSync('/etc/letsencrypt/live/sos.athenssat.fr/cert.pem', 'utf8');
const ca = fs.readFileSync('/etc/letsencrypt/live/sos.athenssat.fr/chain.pem', 'utf8');
const credentials = {
    key: privateKey,
    cert: certificate,
    ca: ca
};
const https = require('https').createServer(credentials, app);


var sess = {
    secret: 'coucou'
};

sql.clear_tokens(DELAI);

app.use(express.static('public')); //gestion des fichiers externes
app.use(bodyParser.urlencoded({extended : true}));//gestion des routes
app.use(session(sess));

app.all("*", ensureSecure);

app.get('/', function (req, res) {
    let date = new Date();

    if (date.getHours() > 0 && date.getHours() < 18) {
        res.render('ntm.ejs');

    } else {
        res.render('index.ejs');
    }
});

app.post('/demande', function (req, res) {
    if(req.body.choice === 'livraison'){
        res.redirect('/livraison');
    }
    else{
        res.redirect('/service');
    }
});

app.post('/livraison', function (req,res) {
    let response_token = JSON.parse(req.body.token);
    token.validate(response_token)
        .then(function (is_valid) {
            if(is_valid === true) {
                let marker = req.body.marker;
                console.log(marker);
                let livraison_id = req.body.command_type;
                let real_name = req.body.real_name;
                let command_name = req.body.command_name;
                let address = req.body.adresse;
                let phone = req.body.phone;
                let commentary = req.body.commentaire;
                sql.insert_dominos(livraison_id, real_name, command_name, address, phone, commentary)
                    .then(function (id) {
                        if(livraison_id == '0'){
                            discord.send_message_dominos(id, real_name, command_name, address, phone, commentary);
                        }
                        else{
                            discord.send_message_kebab(id, real_name, command_name, address, phone, commentary);
                        }
                        res.redirect('/thanks');
                    })
                    .catch(
                        function (err) {
                            console.error(err);
                        }
                    );
            }
            else{
                res.redirect('/');
            }
        });
});

app.post('/service', function (req, res) {
    let response_token = JSON.parse(req.body.token);
    token.validate(response_token)
        .then(function (is_valid) {
            if(is_valid){
                let service_id = req.body.command_type;
                let real_name = req.body.real_name;
                let address = req.body.adresse;
                let phone = req.body.phone;
                let commentary = req.body.commentaire;
                sql.insert_service(service_id, real_name, address, phone, commentary)
                    .then(function (id) {
                        if(id!=-1){
                            switch (service_id) {
                                case '0':
                                    discord.send_message_vaisselle(id, real_name, address, phone, commentary);
                                    break;
                                case '1':
                                    discord.send_message_gare(id, real_name, address, phone, commentary);
                                    break;
                            }
                            res.redirect('/thanks');
                        }
                        else{
                            res.redirect('/error');
                        }
                    })
                    .catch(function (err) {
                        console.error(err);
                    });
            }
            else{
                res.redirect('/');
            }
        });

});

app.get('/home', function (req, res) {
    res.render('home.ejs');
});

app.get('/dominos', function (req,res) {
    res.render('dominos.ejs');
});

app.get('/thanks', function (req,res) {
    res.render('thanks.ejs');
});

app.get('/error', function (req,res) {
    res.render('error.ejs');
});

app.get('/vaisselle', function(req,res){
    res.render('vaiselle.ejs');
});

app.get('/about', function(req,res){
    res.render('about.ejs');
});

//-------------------------------------------------------------------

app.get('/token', function(req, res){
    token.create()
        .then(function (current_token) {
            return res.json(current_token);
        });
});

app.get('/services', function (req, res) {
    sql.get_services()
        .then(function (services_enum) {
            return res.json(services_enum);
        });
});

app.get('/livraisons', function (req, res) {
    sql.get_livraisons()
        .then(function (livraison_enum) {
            return res.json(livraison_enum);
        });
});


//------------
function ensureSecure(req, res, next){
  if(req.secure){
    // OK, continue
    return next();
  };
  // handle port numbers if you need non defaults
  // res.redirect('https://' + req.host + req.url); // express 3.x
  res.redirect('https://' + req.hostname + req.url); // express 4.x
}

//-------------------------------------------------------------------

// HTTP
require("http").createServer(app).listen(8081);

// HTTPS
https.listen(8081,()=>{
    console.log('Serveur 443 lancé.');
});

//------------------------------------------------------------------